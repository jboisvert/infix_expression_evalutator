
package com.jeffreyboisvert.evaluator.buisness;

import java.util.LinkedList;
import java.util.Queue;
import static com.jeffreyboisvert.evaluator.buisness.OperatorWeightConstants.*;
import com.jeffreyboisvert.evaluator.exceptions.DivideByZeroException;
import com.jeffreyboisvert.evaluator.exceptions.InvalidInputStringsInQueueException;
import com.jeffreyboisvert.evaluator.exceptions.NoMatchingParenthesisException;
import com.jeffreyboisvert.evaluator.exceptions.NonBinaryExpressionException;
import java.util.ArrayDeque;
import java.util.Deque;
import org.slf4j.LoggerFactory;

/**
 * This is used to hold useful methods needed to evaluate a infix mathematical expression.
 * This takes a given infix expression (in the form of a Queue of strings) and then converts
 * that given expression into a postfix expression which then calculates the result with. 
 * This object is immutable (meaning a new InfixExpressionEvaluator must be made for a new expression). 
 * 
 * @author Jeffrey Boisvert
 */
public final class InfixExpressionEvaluator implements MathematicalExpressionEvaluator {
    
    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(InfixExpressionEvaluator.class);

    private final Queue<String> expression; 
    
    /**
     * Constructor used to pass the expression being represented
     * 
     * @param expression represented by a queue. 
     */
    public InfixExpressionEvaluator(Queue<String> expression){
        
        this.expression = new LinkedList<>(expression);
        
    }
    
    /**
     * Used to validate the constructed expression. 
     * 
     * @return a double representing the result of the infix expression.  
     * @throws InvalidInputStringsInQueueException when an invalid string is found in expression
     * @throws DivideByZeroException if attempting to divide by 0.
     * @throws com.jeffreyboisvert.evaluator.exceptions.NoMatchingParenthesisException when either a ( or ) is not in a pair. 
     * @throws com.jeffreyboisvert.evaluator.exceptions.NonBinaryExpressionException when trying to do non binary operations
     */
    @Override
    public final double evaluate() throws InvalidInputStringsInQueueException, DivideByZeroException, NoMatchingParenthesisException, NonBinaryExpressionException{
        
        LOG.debug("Evaluating expression " + this.expression + " in InfixExpressionEvaluator");
        
        //Get prefix version of expression
        Queue<String> prefixExpression = convertInfixExpressionToPostfix(this.expression); 
        LOG.debug("Converted infix function " + this.expression + " to prefix expression" + prefixExpression);
        
        double result = evaluatePrefixExpression(prefixExpression);
        LOG.debug("Got a result of " + result);
        
        return result; 
        
    }
    
    /**
     * Used to evaluate a prefix expression for a result.
     * 
     * @param prefixExpression given to be solved
     * @return the number solved from the expression. 
     */
    private double evaluatePrefixExpression(Queue<String> prefixExpression) throws DivideByZeroException, InvalidInputStringsInQueueException{
        
        Deque<Double> operands = new ArrayDeque<>();
        
        while(!prefixExpression.isEmpty()){
            
            String value = prefixExpression.remove();
            
            performEvaluationStep(operands, value);
            
        }
        
        if(operands.size() != 1){
            
            throw new IllegalArgumentException("Operands stack should only contain 1 thing but does not. Stack: " + operands);
            
        }
        
        return operands.pop();
        
    }
    
    /**
     * Used to perform the logic of the step of evaluation for the value given based on the current 
     * operands given from a prefix expression. 
     * 
     * @param operands stack
     * @param value holding either a number or operator. 
     */
    private void performEvaluationStep(Deque<Double> operands, String value) throws DivideByZeroException, InvalidInputStringsInQueueException{
        
        if(this.isNumeric(value)){
            
            operands.push(Double.parseDouble(value));
            
        }
        //An operator
        else {
            
            double rightOperand = operands.pop();
            double leftOperand = operands.pop();
            
            double result = evaluateOperandsAction(leftOperand, rightOperand, value);
            
            operands.push(result);
            
        }
        
    }
    
    /**
     * Used to compute the result based on the two operands and the operator.
     * 
     * @param rightOperand
     * @param leftOperand
     * @param operator to apply to the two operands
     * @return double holding the result of the calculation. 
     */
    private double evaluateOperandsAction(double leftOperand, double rightOperand, String operator) throws DivideByZeroException, InvalidInputStringsInQueueException {
        
        if(rightOperand == 0 && operator.equals("/")){
            
            throw new DivideByZeroException();
            
        }
        
        //Evaluate expression
        switch(operator){
            
            case "+":
                return leftOperand + rightOperand;
                
            case "-":
                return leftOperand - rightOperand;
                
            case "*":
                return leftOperand * rightOperand;
            
            case "/":
                return leftOperand / rightOperand;
                
            default:
                //Incase there is an issue
                throw new InvalidInputStringsInQueueException("Invalid operator " + operator);

        }
        
    }
    
    /**
     * Convert the set infix function into a postfix expression. 
     * 
     * @return a new queue representing the infix expression as a postfix expression.
     * @throws InvalidInputStringsInQueueException when an invalid string is found in expression
     */
    private Queue<String> convertInfixExpressionToPostfix(final Queue<String> infixExpression) throws InvalidInputStringsInQueueException, NoMatchingParenthesisException, NonBinaryExpressionException{
        
        //Used to ensure original remains untouched
        final Queue<String> infixExpressionCopy = new LinkedList<>(infixExpression);
        
        final Queue<String> postfixQueue = new LinkedList<>();
        final Deque<String> operatorsStack = new ArrayDeque<>();
        
        String previousValue = "";
         
        //Loop until all of expression is read
        while(!infixExpressionCopy.isEmpty()){
            
            //Want error thrown if empty means while loop does not work
            String value = infixExpressionCopy.remove();
            
            //Value found is invalid throw exception
            if(!isNumeric(value) && !isOperator(value) && !value.equals("(") && !value.equals(")")){
                
                throw new InvalidInputStringsInQueueException("Invalid string found in queue. Value: " + value);
                
            }
            
            //Means add to postfix queue
            if(isNumeric(value)){
                
                //Previous value was numeric then non binary expression
                if(isNumeric(previousValue)){
                    
                    throw new NonBinaryExpressionException("Attempted to add a number when the previous value was a number: " + this.expression);
                    
                }
                
                //Check if it is ( 2 + 2 ) 4 which means an implied multiplication ( 2 + 2 ) * 4
                if (previousValue.equals(")")){
                    
                    applyPrecedenceLogic(postfixQueue, operatorsStack, "*");
                    
                }
                
                postfixQueue.add(value);
                
            }
            
            //Means values have higher precedence
            else if (value.equals("(")){
                
                //Ensure previousValue is actually assigned something
                if(!previousValue.isBlank()){
                    
                    //If true means must apply a multiplicatiion between brackets
                    if(previousValue.equals(")") || isNumeric(previousValue)){
                        
                        applyPrecedenceLogic(postfixQueue, operatorsStack, "*");
                        LOG.debug("Added multiplier between brackets stack: " + operatorsStack + " prefix " + postfixQueue);
                        
                    }
                    
                }
                
                //Push bracket to stack to begin pair
                operatorsStack.push(value); 
                
            }
            
            //Bracket now needs to be closed
            else if (value.equals(")")){
                
                //Ensure brakcet is recorded.
                previousValue = value;
                
                LOG.debug("Got a ) on and operators are now " + operatorsStack + " and prefix is currently " + postfixQueue);
                
                if(operatorsStack.isEmpty()){
                    
                    throw new NoMatchingParenthesisException("Attempted to close bracket but stack is empty");
                    
                }
                
                //Pop everything off the stack until opening bracket is found
                while(!operatorsStack.isEmpty() && !operatorsStack.peek().equals("(")){
                    
                    postfixQueue.add(operatorsStack.pop());
                    
                }
                
                //Means a opening bracket was never found
                if(operatorsStack.isEmpty()){
                    
                    throw new NoMatchingParenthesisException("Attempted to close bracket with no opening bracket.");
                    
                }
                
                //Remove bracket
                operatorsStack.pop();
                
            }
            
            //Means must find out precedence
            else if(isOperator(value)){
                
                //Means the -1 or +1 is implied on bracket
                if(previousValue.isBlank() && ((value.equals("+") || value.equals("-")) && (infixExpressionCopy.peek().equals("("))) ){
                    
                    String newValue = value + "1";
                    postfixQueue.add(newValue);
                    previousValue = newValue;
                    
                    continue;

                }
                
                //If the previous result is blank or not a number then non binary operation.
                //This does not include ')' since (2+2)*5 is valid.
                if(previousValue.isBlank() || (!isNumeric(previousValue) && !previousValue.equals(")"))){
                    
                    throw new NonBinaryExpressionException("Attempted to add an operator with no operand in front of it: " + this.expression);
                    
                }
                
                //No other operand will follow the operator
                if (infixExpressionCopy.isEmpty()){
                    
                    throw new NonBinaryExpressionException("There is no operand following this operator: " + this.expression);
                    
                }
                
                applyPrecedenceLogic(postfixQueue, operatorsStack, value);
                
            }
            
            //Store to know previous value
            previousValue = value;
            
        }
        
        LOG.debug("Finished contents of postfix and operators are as follows: " + operatorsStack + " : " + postfixQueue);
        
        //Means never was closed
        if(operatorsStack.contains("(") || postfixQueue.contains("(")){
            
            throw new NoMatchingParenthesisException("No closing bracket! operator stack: " + operatorsStack + " postifx: " + postfixQueue);
            
        }
        
        //Add remaining operators
        while(!operatorsStack.isEmpty()){
            
            postfixQueue.add(operatorsStack.pop());
            
        }
        
        return postfixQueue;
        
    }
    
    /**
     * Used to handle the logic of either adding operand to the operators stack, or if it holds higher precedence than
     * what is on the stack, to then pop what is off the stack into the postfix queue and then add it to the stack. 
     * 
     * @param postfixQueue holding the postfix expression built so far
     * @param operatorsStack holding operators
     * @param operator to be checked
     */
    private void applyPrecedenceLogic(Queue<String> postfixQueue, Deque<String> operatorsStack, String operator){
        
        //Add to stack if empty or if the operator has higher precedence over the operator at top of stack
        if(operatorsStack.isEmpty()){
            
            LOG.debug("Stack was empty adding " + operator);
            
            operatorsStack.push(operator);
            
        }
        else if (!operatorsStack.isEmpty()){
            
             if(calculateOperatorWeight(operator) > calculateOperatorWeight(operatorsStack.peek())){
                 
                 LOG.debug(operator + " has higher precedence than " + operatorsStack.peek());
                 
                operatorsStack.push(operator);
                
             }
             else {
            
             LOG.debug(operator + " does not have higher precedence than " + operatorsStack.peek());
                 
            //Keep going through stack until either the stack is empty or while operand has equal or less precedence and is not a opening bracket
            while(!operatorsStack.isEmpty() && (calculateOperatorWeight(operator) <= calculateOperatorWeight(operatorsStack.peek())) && !operatorsStack.peek().equals("(")){
                
                String operatorFromStack = operatorsStack.pop();
                postfixQueue.add(operatorFromStack);
                
            }
            
            operatorsStack.push(operator);
            
            LOG.debug("Now " + operator + " is added to stack. Now stack is " + operatorsStack);
            
        }

            
        }
        
    }
    
    /**
     * Used to validate if a string is numeric. 
     * 
     * @param string to check
     * @return true if it is numeric and false otherwise
     */
    private boolean isNumeric(final String string) {

        // null or empty
        if (string == null || string.length() == 0) {
            
            return false;
            
        }
        
        try {
            
            Double.parseDouble(string);
            
        } catch (NumberFormatException ex) {
            
            return false;
            
        }
        
        //Can only get here if exception is not thrown
        return true;

    }
    
    /**
     * Used to know if the given string is an operator
     * 
     * @param string holding an operator
     * @return 
     */
    private static boolean isOperator(String string) {
        
        return string.equals("+") || string.equals("-") || string.equals("*") || string.equals("/") || string.equals("(") || string.equals(")");
        
    }
    
    /**
     * Used to validate the weight of a given operator. 
     * For example, * has a higher weight then +. 
     * 
     * @param operator given. 
     * @return weight value of the operator.
     * @throws IllegalArgumentException if the operator is not an expected operator. 
     */
    private int calculateOperatorWeight(String operator){
        
        switch (operator) 
        { 
            case "+": 
            case "-": 
                return ADDITION_SUBTRACTION_WEIGHT; 
       
            case "*": 
            case "/": 
                return DIVISION_MULTIPLICATION_WEIGHT;  
                
            case "(":
            case ")":
                return BRACKET_WEIGHT;
                
            default: 
                throw new IllegalArgumentException("Invalid string given for operator: " + operator);
        
        } 
        
    }

    /**
     * Used to get a copy of the queue holding the infix expression.
     * 
     * @return queue of type string that is a copy of the expression. 
     */
    @Override
    public final Queue<String> getExpression() {
        
        return new LinkedList<>(this.expression);
        
    }
    
}
