
package com.jeffreyboisvert.evaluator.buisness;

/**
 * Used to hold the weights of mathematical operators.
 * This weight is based on BEDMAS. 
 * 
 * @author Jeffrey Boisvert
 */
public class OperatorWeightConstants {
    
    public static final int BRACKET_WEIGHT = 4;
    public static final int EXPONENT_WEIGHT = 3; 
    public static final int DIVISION_MULTIPLICATION_WEIGHT = 2; 
    public static final int ADDITION_SUBTRACTION_WEIGHT = 1;
    
}
