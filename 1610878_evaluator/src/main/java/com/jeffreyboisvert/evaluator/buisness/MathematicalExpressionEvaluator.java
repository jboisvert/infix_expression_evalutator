package com.jeffreyboisvert.evaluator.buisness;

import com.jeffreyboisvert.evaluator.exceptions.DivideByZeroException;
import com.jeffreyboisvert.evaluator.exceptions.InvalidInputStringsInQueueException;
import com.jeffreyboisvert.evaluator.exceptions.NoMatchingParenthesisException;
import com.jeffreyboisvert.evaluator.exceptions.NonBinaryExpressionException;
import java.util.Queue;

/**
 * Useful interface abstraction used to create objects used to validate expressions in the form of a Queue of type string. 
 * 
 * @author Jeffrey Boisvert
 */
public interface MathematicalExpressionEvaluator {

	double evaluate() throws InvalidInputStringsInQueueException,
	DivideByZeroException,
	NoMatchingParenthesisException,
	NonBinaryExpressionException;

	Queue < String > getExpression();

}