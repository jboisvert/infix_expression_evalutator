
package com.jeffreyboisvert.evaluator.exceptions;

/**
 * To be used when an expression contains non-binary expressions such as 4,*,3,+.
 * @author Jeffrey Boisvert
 */
public class NonBinaryExpressionException extends Exception {
    
    private final static String DEFAULT_MESSAGE = "Queue must contain binary expressions (ex: 2+2 and not 2+)";
    
    /**
     * Default constructor
     */
    public NonBinaryExpressionException(){
        
        this(DEFAULT_MESSAGE);
        
    }
    
    /**
     * Used to let the user give their own message. 
     * 
     * @param message given
     */
    public NonBinaryExpressionException(final String message){
        
        super(message);
        
    }
    
}
