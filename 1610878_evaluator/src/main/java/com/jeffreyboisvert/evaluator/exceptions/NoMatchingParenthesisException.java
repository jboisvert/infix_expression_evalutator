
package com.jeffreyboisvert.evaluator.exceptions;

/**
 * Used for when an open or closed parenthesis is not in a pair. 
 * 
 * @author Jeffrey Boisvert
 */
public class NoMatchingParenthesisException extends Exception {
    
    private final static String DEFAULT_MESSAGE = "Parenthesis is not paired with another parenthesis.";
    
    /**
     * Default constructor
     */
    public NoMatchingParenthesisException(){
        
        this(DEFAULT_MESSAGE);
        
    }
    
    /**
     * Used to let the user give their own message. 
     * 
     * @param message given
     */
    public NoMatchingParenthesisException(final String message){
        
        super(message);
        
    }
    
}
