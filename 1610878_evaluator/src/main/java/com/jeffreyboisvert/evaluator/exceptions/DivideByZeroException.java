
package com.jeffreyboisvert.evaluator.exceptions;

/**
 * Used to represent an issue when attempting to divide a number by zero
 * 
 * @author Jeffrey Boisvert
 */
public class DivideByZeroException extends Exception {
    
    private final static String DEFAULT_MESSAGE = "Divisor cannot be zero";
    
    /**
     * Default constructor
     */
    public DivideByZeroException(){
        
        this(DEFAULT_MESSAGE);
        
    }
    
    /**
     * Used to let the user give their own message. 
     * 
     * @param message given
     */
    public DivideByZeroException(final String message){
        
        super(message);
        
    }
    
}
