
package com.jeffreyboisvert.evaluator.exceptions;

/**
 * Used for when there are invalid string identified in a queue. 
 * 
 * @author Jeffrey Boisvert
 */
public class InvalidInputStringsInQueueException extends Exception {
    
    private final static String DEFAULT_MESSAGE = "Queue contains invalid strings";
    
    /**
     * Default constructor
     */
    public InvalidInputStringsInQueueException(){
        
        this(DEFAULT_MESSAGE);
        
    }
    
    /**
     * Used to let the user give their own message. 
     * 
     * @param message given
     */
    public InvalidInputStringsInQueueException(final String message){
        
        super(message);
        
    }
    
}
