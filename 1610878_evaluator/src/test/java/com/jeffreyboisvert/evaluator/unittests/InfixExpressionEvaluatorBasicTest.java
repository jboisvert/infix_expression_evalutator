
package com.jeffreyboisvert.evaluator.unittests;

import com.jeffreyboisvert.evaluator.buisness.InfixExpressionEvaluator;
import com.jeffreyboisvert.evaluator.buisness.MathematicalExpressionEvaluator;
import com.jeffreyboisvert.evaluator.exceptions.DivideByZeroException;
import com.jeffreyboisvert.evaluator.exceptions.InvalidInputStringsInQueueException;
import com.jeffreyboisvert.evaluator.exceptions.NoMatchingParenthesisException;
import com.jeffreyboisvert.evaluator.exceptions.NonBinaryExpressionException;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Used to test the InfixExpressionEvaluator class. 
 * This class handles doing basic tests on the evaluator to ensure the get method works as intended
 * and the correct results are found. 
 * 
 * @author Jeffrey Boisvert
 */
@RunWith(Parameterized.class)
public class InfixExpressionEvaluatorBasicTest {
        
    Queue<String> expression; 
    double expectedResult; 
    
    /**
     * A static method is required to hold all the data to be tested and the
     * expected results for each test. This data must be stored in a
     * two-dimension array. The 'name' attribute of Parameters is a JUnit 4.11 feature
     *
     * @return The list of arrays
     * @throws java.io.IOException when the file used for tests is not found
     */
    @Parameters(name = "{index} expression: {0} = {1} ]")
    public static Collection<Object[]> data() throws IOException {
        
         try (Reader reader = Files.newBufferedReader(Paths.get("src/test/resources/valid-infix-expressions.csv"))){
                 
            CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT.withHeader("equation", "expected_result"));
            List<CSVRecord> records = csvParser.getRecords();
            
            //Not efficent but removes the first un-needed line
            records.remove(0);
            
            Collection<Object[]> dataToReturn = new ArrayList<>();
            
             //Go through each record
             records.forEach((csvRecord) -> {
                 String equation = csvRecord.get("equation");
                 
                 //CSV must have spaces inbetween parts of expression
                 Queue infixQueue = new LinkedList<>(Arrays.asList(equation.split(" ")));
                 
                 Double expectedResult = Double.parseDouble(csvRecord.get("expected_result"));
                 
                 //Add new parameters
                 dataToReturn.add( new Object[]{infixQueue, expectedResult} );
             });
            
            return dataToReturn;
            
        }
         
    }
    
    /**
     * Constructor that receives all the data for each test as defined by a row
     * in the list of parameters. 
     * 
     * @param expression represented by a queue
     * @param expectedResult of the expression. 
     */
    public InfixExpressionEvaluatorBasicTest(final Queue<String> expression, final double expectedResult){
        
        this.expression = expression; 
        this.expectedResult = expectedResult; 
        
    }
    
    /**
     * Used to test the queue of strings given is still can be accessed
     */
    @Test
    public void testGetExpressionIsSameAsOriginal() {
        
        //Given
        MathematicalExpressionEvaluator evaluator = new InfixExpressionEvaluator(this.expression);
        
        //When
        Queue<String> expressionRecieved = evaluator.getExpression();

        //Then
        assertEquals(this.expression, expressionRecieved);
        
    }
    
    /**
     * Used to test the queue of strings given is a deep copy of the queue. 
     */
    @Test
    public void testGetExpressionDoesNotAffectEvaluatorExpression() {
        
        //Given
        MathematicalExpressionEvaluator evaluator = new InfixExpressionEvaluator(this.expression);
        
        //When
        Queue<String> expressionRecieved = evaluator.getExpression();
        expressionRecieved.clear();
        Queue<String> secondExpressionRecieved = evaluator.getExpression();

        //Then
        assertNotEquals(expressionRecieved, secondExpressionRecieved);
        
    }
    
    /**
     * Used to test if the given expression evaluates the correct result.Exceptions are just put to allow tests to run
     * 
     * @throws com.jeffreyboisvert.evaluator.exceptions.InvalidInputStringsInQueueException which is not expected
     * @throws com.jeffreyboisvert.evaluator.exceptions.DivideByZeroException which is not expected
     * @throws com.jeffreyboisvert.evaluator.exceptions.NoMatchingParenthesisException which is not expected
     * @throws com.jeffreyboisvert.evaluator.exceptions.NonBinaryExpressionException which is not expected
     */
    @Test
    public void testEvaluateGivesTheCorrectResult() throws InvalidInputStringsInQueueException, DivideByZeroException, NoMatchingParenthesisException, NonBinaryExpressionException {
        
        //Given
        MathematicalExpressionEvaluator evaluator = new InfixExpressionEvaluator(this.expression);
        
        //When
        double result = evaluator.evaluate();

        //Then
        assertEquals(this.expectedResult, result, 0.01);
        
    }
    
}
