
package com.jeffreyboisvert.evaluator.unittests;

import com.jeffreyboisvert.evaluator.buisness.InfixExpressionEvaluator;
import com.jeffreyboisvert.evaluator.buisness.MathematicalExpressionEvaluator;
import com.jeffreyboisvert.evaluator.exceptions.DivideByZeroException;
import com.jeffreyboisvert.evaluator.exceptions.InvalidInputStringsInQueueException;
import com.jeffreyboisvert.evaluator.exceptions.NoMatchingParenthesisException;
import com.jeffreyboisvert.evaluator.exceptions.NonBinaryExpressionException;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 * Used to test that an exception is thrown when a closing bracket is not closed. 
 * 
 * @author Jeffrey Boisvert
 */
@RunWith(Parameterized.class)
public class InfixExpressionEvaluatorNoMatchingParenthesisExceptionTest {
    
    Queue<String> expression; 
    
    /**
     * A static method is required to hold all the data to be tested and the
     * expected results for each test. This data must be stored in a
     * two-dimension array. The 'name' attribute of Parameters is a JUnit 4.11 feature
     *
     * @return The list of arrays
     * @throws java.io.IOException when the file used for tests is not found
     */
    @Parameterized.Parameters(name = "{index} expression: {0}")
    public static Collection<Object[]> data() throws IOException {
        
         try (Reader reader = Files.newBufferedReader(Paths.get("src/test/resources/no-closing-bracket-in-infix-expressions.csv"))){
                 
            CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT.withHeader("equation"));
            List<CSVRecord> records = csvParser.getRecords();
            
            //Not efficent but removes the first un-needed line
            records.remove(0);
            
            Collection<Object[]> dataToReturn = new ArrayList<>();
            
             //Go through each record
             records.forEach((csvRecord) -> {
                 String equation = csvRecord.get("equation");
                 
                 //CSV must have spaces inbetween parts of expression
                 Queue infixQueue = new LinkedList<>(Arrays.asList(equation.split(" ")));
                 
                 //Add new parameters
                 dataToReturn.add( new Object[]{infixQueue} );
             });
            
            return dataToReturn;
            
        }
         
    }
    
    /**
     * Constructor that receives all the data for each test as defined by a row
     * in the list of parameters. 
     * 
     * @param expression represented by a queue 
     */
    public InfixExpressionEvaluatorNoMatchingParenthesisExceptionTest(final Queue<String> expression){
        
        this.expression = expression; 
        
    }
    
    /**
     * Used to test that the given expression throws the error when brackets do not have matching pairs.
     * 
     * @throws com.jeffreyboisvert.evaluator.exceptions.InvalidInputStringsInQueueException which is not expected.
     * @throws com.jeffreyboisvert.evaluator.exceptions.DivideByZeroException which is not expected.
     * @throws com.jeffreyboisvert.evaluator.exceptions.NoMatchingParenthesisException which is expected
     * @throws com.jeffreyboisvert.evaluator.exceptions.NonBinaryExpressionException which is not expected
     */
    @Test(expected=NoMatchingParenthesisException.class) 
    public void testExpressionThrowsNoMatchingParenthesisException() throws InvalidInputStringsInQueueException, DivideByZeroException, NoMatchingParenthesisException, NonBinaryExpressionException {
        
        //Given
        MathematicalExpressionEvaluator evaluator = new InfixExpressionEvaluator(this.expression);
        
        //When
        evaluator.evaluate();
        
    }
    
}
