# Infix Expression Evaluator

Project to convert an infix expression to a postfix expression and then evaluates the postfix expression to give a result.

## How to use

* Fork or clone the repo. 
* You will then have access to the classes which you can use. However, if you wish to test, with Maven run the project with a "test" goal to see the unit tests run.

## Notes

* InfixExpressionEvaluator is an immutable object meaning you must create a new one for each Queue<String> expression (based off behaviour of String)
* In the test resources there are CSVs that are used to run the unit tests. Expressions have spaces inbetween just for parsing reasons (ex: 2+2 is not correct it should be 2 + 2)
* Square root is not supported neither is power of (not within the scope of the assignment). 

 